const express = require("express");
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

//DB Config with mLab
const db = require('./config/keys').mongoURI;

//Connect to MongoDB:
mongoose.connect('mongodb://localhost/database' && db, { useNewUrlParser: true })
    .then(() => console.log("Mongo DB connected"))
    .catch(err => console.log(err));
mongoose.Promise = global.Promise;

//Middlewares: 


//Serve static HTML:
app.use(express.static('./public'))
//Express built-in middleware body-parser
//app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json())
//Using the route handlers
app.use('/api/shopping', require('./routes/api/shopping'));



//Listening for requests on a port:
const port = process.env.PORT || 4002;

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});