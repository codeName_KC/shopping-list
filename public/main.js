let shopping_list; 
let onDelete; 
let onSave; 

// Getting the list of shopping items: 
 fetch('http://localhost:4002/api/shopping/list').then( (items) => {   
 return items.json(); 
}).then( (itemsList) => {

     shopping_list = itemsList; 
     
     // Markup for empty table
      const table = document.getElementById("table-list");
      const header_row = table.insertRow(0);
      const cell1 = header_row.insertCell(0);
      const cell2 = header_row.insertCell(1);
      const cell3 = header_row.insertCell(2);
      cell1.innerHTML = "Shopping Item";
      cell2.innerHTML = "Quantity";
      
      cell1.className = "p-2";
        // Outputting the shopping list to the table
        shopping_list.forEach( (item) => {

        let index = 0;

        const row = table.insertRow(index - 1);
        const cell_1 = row.insertCell(index);
        const cell_2 = row.insertCell(index + 1);
        const cell_3 = row.insertCell(index + 2);
       

        cell_1.innerHTML = `<p>${item.item_name}<p/>`;
        cell_2.innerHTML = `<input class="table rounded" value="${item.quantity}"/>`;
      
        cell_2.className = 'd-flex flex-row no-border justify-items-center align-items-center'
        // Generating delete buttons for each row
        const deleteButton = document.createElement('button'); 
        deleteButton.className = "btn";
        deleteButton.innerHTML = 'Delete'; 
        cell_3.appendChild(deleteButton); 
        deleteButton.onclick = onDelete;

        // Generating saved buttons for each row
      const savedButton = document.createElement('button'); 
      savedButton.className = "btn"; 
      savedButton.innerHTML = 'Save'; 
      cell_3.appendChild(savedButton); 
      savedButton.onclick = onSave;

        index++; 
        });
        return shopping_list; 
});

// Delete button handler
onDelete = (e) => {
   e.preventDefault();

   // event.target is the input element.
   const td = e.target.parentNode; 
   const tr = td.parentNode; // the row to be removed
    
   let id; 

   // Getting the id of the item for the targeted row: 
   const selected_item = shopping_list.find( item =>
         item.item_name === tr.childNodes[0].firstChild.value
   )

   id = selected_item._id; 

   // Delete the item and its table row 
   fetch(`http://localhost:4002/api/shopping/remove-item/${id}`, {
            method: 'DELETE',
            headers:{
            'Content-Type': 'application/json'
            }
      }).then( (items) => {
            // Remove row containing the item
            tr.parentNode.removeChild(tr);
      }).catch( (err) => {
            console.error('Error:', err);
      })
}


// Adding an item to the shopping list:
const form = document.getElementById('add-item'); 

form.onsubmit = function(e) {
    e.preventDefault();

    const item_name = form.item_name.value; 
    const quantity = form.quantity.value;
  
    const item = {
        item_name,
        quantity,
    }

  // Adding an item to the database: 
  fetch('http://localhost:4002/api/shopping/add-item', {
          method: 'POST',
          body: JSON.stringify(item), 
          headers:{
          'Content-Type': 'application/json'
          }
      }).then(res => { 
         return res.json(); 
      }).then((item) => {
         // Generating new row for the item:
         const table = document.getElementById("table-list");
         const row = table.insertRow(-1);
         const cell_1 = row.insertCell(0);
         const cell_2 = row.insertCell(1);
         const cell_3 = row.insertCell(2);

         shopping_list.push(item); 
         cell_1.innerHTML = `<p> ${item.item_name} </p>`;
         cell_2.innerHTML = `<input class="table" value="${item.quantity}"/>`;
        
         cell_2.className = 'd-flex flex-row no-border'
        // Generating delete button for new row
        const button = document.createElement('button'); 
        button.className = "btn";
        button.innerHTML = 'Delete'; 
        cell_3.appendChild(button); 
        button.onclick = onDelete;

      // Generating save buttons for each row
      const savedButton = document.createElement('button'); 
      savedButton.className = "btn"; 
      savedButton.innerHTML = 'Save'; 
      cell_3.appendChild(savedButton); 
      savedButton.onclick = onSave;

      }).catch(error => console.error('Error:', error));
    
    form.reset(); 
}


// Save button handler
onSave = (e) => {
      e.preventDefault(); 

      // event.target is the input element.
      const td = e.target.parentNode; 
      const tr = td.parentNode; // the row to be updated 

      // Getting the id of the item in the targeted row: 
      let id;
      const saved_item = shopping_list.find( item =>
            item.item_name === tr.childNodes[0].firstChild.innerHTML
      )

      id = saved_item._id; 
      
      const updated_item = {
            //item_name: tr.childNodes[0].firstChild.value,
            quantity: tr.childNodes[1].firstChild.value,
      }
      
      fetch(`http://localhost:4002/api/shopping/update-item/${id}`, {
            method: 'PUT',
            body: JSON.stringify(updated_item), 
            headers:{
            'Content-Type': 'application/json'
            }
      }).then( (item) => {
            
      }).catch( (err) => {
            console.error('Error:', err);
      })
}