const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Shopping_itemSchema = new Schema({
    item_name : {
        type: String,
        required: [true, 'this field is required']
    },
    quantity: {
        type: Number,
        required: [true, 'this field is required']
    }
});

module.exports = Shopping_item = mongoose.model('shopping', Shopping_itemSchema);