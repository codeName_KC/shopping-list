const express = require('express');
const router = express.Router();
const Shopping_item = require('../../models/Shopping-item');

// List of all items. GET Request '/api/shopping'
router.get('/list', (req, res, next) => {
  Shopping_item.find()/*.sort('item_name', 'descending')*/.then( (items) => {
    res.send(items).json(); 
  }).catch((err) => {
    console.log(err);
})
}); 


// Add item 
router.post('/add-item', (req, res, next) => {
    /*
    const new_item = new Shopping_item({
        item_name: req.body.item_name,
        quantity: req.body.quantity,
    });

    new_item.save()
    .then(item => res.json(item))
    */
    Shopping_item.create(req.body)
    .then((item) => {
        res.json(item);
    })
    .catch((err) => {
        console.log(err);
    })
}); 

// Update item
router.put('/update-item/:id', (req, res, next) => {
    Shopping_item.findOneAndUpdate({
        _id: req.params.id
    }, req.body).then(() => {
        Shopping_item.findOne({ _id: req.params.id })
            .then((item) => {
                res.json(item);
            })
        }).catch(next);
}); 

// Delete item
router.delete('/remove-item/:id', (req, res, next) => {
    Shopping_item.findOneAndRemove({ _id: req.params.id })
    .then((item) => {
        res.send(item);
    }).catch(err => res.status(404).json({ valid_id: false }));
}); 


// Delete ALL items
router.delete('/remove-items', (req, res, next) => {
    Shopping_item.find()
    .then((items) => {
        res.send('Cleared all items');
    }).catch(err => res.status(404).json({ valid_id: false }));
}); 

module.exports = router;